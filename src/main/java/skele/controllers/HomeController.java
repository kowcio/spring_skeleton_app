package skele.controllers;

import java.util.Date;
import java.util.Map;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller 
public class HomeController extends AbstractController {

	
	
	
	@RequestMapping("/")
	public String mainPage(Model model) {
		
		System.err.println("SER 3E23");
		try {
	        UserDetails userDetails =
	        		(UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        System.err.println("USER 3Ew" + userDetails.getUsername());
	        userDetails.getAuthorities().forEach(auth -> System.err.println(auth.getAuthority().toString()));
        } catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		
		
		model.addAttribute("title", "Hello Home");
		model.addAttribute("date", new Date());
		return PAGES_MAIN_PAGE;
	}
	

	@RequestMapping("/login")
	public String home(Map<String, Object> model) {
		model.put("message", "Hello World");
		model.put("title", "Hello Home");
		model.put("date", new Date());
		return LOGIN_PAGE;
	}
	
	@RequestMapping("user")
	@ResponseBody
	@Secured("ROLE_USER")
	public String test(Model model) {
		return "ROLE_USER";
	}
	
	@RequestMapping("admin")
	@ResponseBody
	@Secured("ROLE_ADMIN")
	public String ad(Model model) {
		return "ROLE_ADMIN";
	}
	@RequestMapping("all")
	@ResponseBody
	public String all(Model model) {
		return " all, no secured";
	}
	
}
