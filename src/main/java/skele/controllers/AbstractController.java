package skele.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.fest.assertions.Assertions.assertThat;


/**
 * Base class for controllers
 *
 */
public abstract class AbstractController {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    //PAGES
    
    public static final String LOGIN_PAGE = "/login";
    public static final String LOGIN_ERROR_PAGE = LOGIN_PAGE+"/error";
    public static final String PANEL_HOME_PAGE = "/main";
    public static final String HOME_PAGE = "/main";
    
    public static final String PAGES = "/pages";
    public static final String PAGES_MAIN_PAGE = PAGES+"/main";

    //TEMPLATES
    public static final String LOGIN_TEMPLATE = "login";


    public static final int PAGE_SIZE = 20; 

    /**
     * Obsługa wyjątków w aplikacji
     *
     * @param request
     * @param response
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    protected ModelAndView handleException(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        logger.error("Unexpected system error: " + ex.getMessage(), ex);

        // wyloguj użytkownika
        logout(request, response);

        // przekieruj na stronę błędu z komunikatem
        ModelAndView mav = new ModelAndView();
        String message = "Wystąpił nieoczekiwany błąd systemu.";
        if (ex instanceof AccessDeniedException) {
            message = "Brak dostępu!";
        }
        mav.addObject("error", message);
        mav.setViewName(LOGIN_TEMPLATE);
        return mav;
    }

    /**
     * Wylogowanie użytkownika z systemu
     *
     * @param request
     * @param response
     */
    protected void logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

    /**
     * Pomocnicza metoda do przekierowań na konkretną stronę.
     *
     * @param page
     * @return
     */
    protected String redirect(String page) {
        assertThat(page).isNotEmpty();
        return "redirect:"+page;
    }
}
